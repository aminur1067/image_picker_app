import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ImageShowcase extends StatelessWidget {
  List<String> imagePath;
  final   Function(int ) onDelete;

  ImageShowcase({
    Key? key,
    required this.imagePath,required this.onDelete,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final imageWidget = <Widget>[];
    for (var i = 0; i < imagePath.length; i++) {
      imageWidget.add(
        SingleImageShowcase(

          onDetele: onDelete!=null ?()=>onDelete(i):null!,

          imagePath: imagePath[i],

        ),
      );
    }
    if (imagePath.isEmpty) {
      return Container(
        child: Text('no image'),
      );
    }
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: imageWidget,
      ),
    );
  }
}

class SingleImageShowcase extends StatelessWidget {
  final String imagePath;
  final VoidCallback onDetele;

  const SingleImageShowcase({Key? key, required this.imagePath,required this.onDetele})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(padding: EdgeInsets.all(8.0),
      child:Stack(
        children: [
          SizedBox(height: 200,width: 200,
          child: Image.file(File(imagePath),fit: BoxFit.cover,),),

          Positioned(right: 0,child: FloatingActionButton(
            onPressed:  onDetele,backgroundColor: Colors.red,
            child: Icon(Icons.delete),
          ),)

        ],
      ),);
  }
}
